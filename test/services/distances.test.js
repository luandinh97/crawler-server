const assert = require('assert');
const app = require('../../src/app');

describe('\'distances\' service', () => {
  it('registered the service', () => {
    const service = app.service('distances');

    assert.ok(service, 'Registered the service');
  });
});
