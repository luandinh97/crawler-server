const assert = require('assert');
const app = require('../../src/app');

describe('\'crawler-devices\' service', () => {
  it('registered the service', () => {
    const service = app.service('crawler-devices');

    assert.ok(service, 'Registered the service');
  });
});
