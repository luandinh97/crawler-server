const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const getUncrawledDistances = require('../../src/hooks/get-uncrawled-distances');

describe('\'get-uncrawled-distances\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: getUncrawledDistances()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
