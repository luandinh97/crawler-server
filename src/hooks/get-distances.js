// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  let distanceList = [];
  return async context => {
    const { result } = context;
    const { Model } = context.app.service('distances')
    let myresult;
    if (distanceList.length === 0) {
      distanceList = await Model.find({});
      await distanceList.sort(function (a, b) { return 0.5 - Math.random() });
      myresult = await distanceList.splice(0, 4);
    }
    else {
      myresult = distanceList;
      // await distanceList.splice(0,4);
      distanceList = [];
      var d = new Date();
      console.log('>>>>>>>Distance list was resetted in', d.toString());
    }
    context.result = myresult;
    return context;
  };
}
    
