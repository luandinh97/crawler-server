// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  let uncrawledDistanceList = [];
  return async context => {
    const { result } = context;
    let myresult;

    //find devices that remains distances more than 15 mins
    let fifteenMinsAgo = new Date((new Date()).valueOf() - 1000*60*15).toISOString();
    let unfinishedDevices = await context.app.service('crawler-devices').Model.find({
      "StartTime": {$lt: fifteenMinsAgo}
    })
    await context.app.service('crawler-devices').Model.deleteMany({
      "StartTime": {$lt: fifteenMinsAgo}
    })
    //get all distanceID of uncrawled ones
    let uncrawledDistanceArrID = [];
    if(unfinishedDevices.length > 0)
    {
      for (let i = 0; i < unfinishedDevices.length; i++) {
        for (let j = 0; j < unfinishedDevices[i].DistanceList.length; j++) {
          uncrawledDistanceArrID.push(unfinishedDevices[i].DistanceList[j]);
        }
      }
    }
    //then delete all the devices above from db

    //get all distances by distanceIDs above
    uncrawledDistanceList = await context.app.service('distances').Model.find({
      "id" : {$in : uncrawledDistanceArrID}
    })
  
    await uncrawledDistanceList.sort(function (a, b) { return 0.5 - Math.random() });
    if (uncrawledDistanceList.length > 4) {
      myresult = await uncrawledDistanceList.splice(0, 4);
    }
    else
      myresult = uncrawledDistanceList;
    context.result = myresult;
    return context;
  };
};
