// Initializes the `distances` service on path `/distances`
const createService = require('feathers-mongoose');
const createModel = require('../../models/distances.model');
const hooks = require('./distances.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/distances', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('distances');

  service.hooks(hooks);
};
